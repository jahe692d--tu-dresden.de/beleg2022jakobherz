﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Beleg2022
{
    /// <summary>
    /// Dies ist der Einstiegspunkt für das Progamm.
    /// Bitte ändern Sie hier nichts.
    /// Durch Schließen des Fensters beenden Sie das Programm.
    /// </summary>
    /// ///
    public class Program
    {
        public static void Main(string?[]? args)
        {
         
            // Achten Sie darauf, bei der Pfadangabe, keine absoluten
            // (c:User/IhrName/) Pfade zu verwenden, vermeiden Sie auch
            // Bachslashes ( \ ) da diese ausschließlich auf Windows
            // funktionieren, Ihre Abgabe aber nicht auf Windows erfolgt!
            
            Beleg2022.Abteilungssteuerung verwaltung = new Abteilungssteuerung(@"../../../Konfiguration.csv");            
            

#if DEBUG
            Console.WriteLine("\nProgram exits. Push any key to continue ...");
            Console.ReadKey();
#endif
        }
    }

    public abstract class Lager : Produktionseinrichtung
    {
        protected static int _Kapazitaet = 20;
        protected Queue<Teil> _Bestand = new Queue<Teil>();
        public Lager(string name, string pos) : base(name, pos) { }

        public override Status ErmittleStatus()
        {
            if (_Bestand.Count == _Kapazitaet) { _AktuellerStatus = Status.ABHOLBEREIT; }
            if (_Bestand.Count == 0) { _AktuellerStatus = Status.EMPFANGSBEREIT; }
            if (_Bestand.Count < _Kapazitaet && _Bestand.Count != 0) { _AktuellerStatus = Status.INTERAKTIONSBEREIT; }
            return _AktuellerStatus;
        }

        public bool EmpfangeTeil(Teil teil)
        {
            if (GetStatus() == Status.INTERAKTIONSBEREIT || GetStatus() == Status.EMPFANGSBEREIT)
            else { _Internal.Ausgabe("Lager voll!"); return false; }
            List
        }
    }

    public class Fertigungsinsel : Produktionseinrichtung
    {
        private Teil _Aktuelles Teil
        private DateTime _BelegtBis;
        public Fertigungsinsel(string name, string pos) : base(name, pos) { }

        public Teil GetTeil()
        {
            if (_AktuellerStatus == Status.ABHOLBEREIT)
            {
                _AktuellerStatus = Status.EMPFANGSBEREIT;
                Teil a = _AktuellesTeil;
                _AktuellesTeil = null;
                return a;
            }
            return null;
        }

        public bool EmpfangeTeil(Teil teil)
        {
            if (_AktuellerStatus != Status.EMPFANGSBEREIT) { return false; }
            if (teil == null) { return false; }
            if (HatFaehigkeit(teil.GetNaechstenSchritt())) { BearbeiteTeil(); _AktuellerStatus = Status.BELEGT; _AktuellesTeil = teil; return true; }
            return false;
        }

        private void BearbeiteTeil()
        {
            _BelegtBis = DateTime.Now + TimeSpan.FromSeconds(10);
        }

        public override Status ErmittleStatus()
        {
            if (_AktuellerStatus == Status.BELEGT && DateTime.Now > _BelegtBis)
            {
                _AktuellesTeil.EntferneFertigenSchritt();

                if (HatFaehigkeit(_AktuellesTeil.GetNaechstenSchritt()))
                {
                    BearbeiteTeil();
                }
                else { _AktuellerStatus = Status.ABHOLBEREIT; }
            }
            return _AktuellerStatus;
        }
    }

    public class Transportfahrzeug : Transportsystem
    {
        private Teil _AktuellesTeil;
        public Transportfahrzeug(string name, string pos) : base(name, pos) { }
        public override void Hauptprozess(List<Produktionseinrichtung> pe)
        {
            int _IndexEingangslager = 3;
            int _IndexInsel1 = 0;
            int _IndexInsel2 = 1;
            int _IndexInsel3 = 2;
            int _IndexAusgangslager = 4;
            bool Ende = false;
            while (Ende == false)
            {
                if (pe.Find(x => x.ErmittleStatus() == Status.ABHOLBEREIT) != null && _AktuellesTeil == null)
                {
                    Produktionseinrichtung ziel = pe.Find(x => x.ErmittleStatus() == Status.ABHOLBEREIT);
                    _Internal.Ausgabe("Ziel gefunden:" + ziel);
                    FahreZu(ziel.GetPosition());
                    _Internal.Ausgabe("fahre zu:" + ziel);

                    _Internal.Ausgabe("naechsen Schritt lesen");
                    if (_AktuellesTeil.GetNaechstenSchritt() == Verarbeitungsschritt.EINLAGERN)
                    {
                        FahreZu(pe[_IndexAusgangslager].GetPosition());
                    }
                    else
                    {
                        Produktionseinrichtung _FertigungsinselMitFaehigkeit = pe.Find(x => x.HatFaehigkeit(_AktuellesTeil.GetNaechstenSchritt()) == true);
                        _Internal.Ausgabe("suche Fertigungsinsel mit Faehigkeit");
                        if (_FertigungsinselMitFaehigkeit.GetStatus() == Status.EMPFANGSBEREIT)
                        {
                            FahreZu(_FertigungsinselMitFaehigkeit.GetPosition());
                            _Internal.Ausgabe("Fahre zu Fertigungsinsel mit Faehigkeit");
                        }
                        if (_FertigungsinselMitFaehigkeit.GetStatus() != Status.EMPFANGSBEREIT && pe[_IndexEingangslager].GetStatus() != Status.ABHOLBEREIT)
                        {
                            FahreZu(pe[_IndexEingangslager].GetPosition());
                            _Internal.Ausgabe("keine Fertigungsinsel EMPFANGSBEREIT, fahre zu Eingangslager");
                        }
                        if (pe[_IndexAusgangslager].GetStatus() != Status.ABHOLBEREIT) { FahreZu(pe[_IndexAusgangslager].GetPosition()); }
                        else { Ende = true; }
                    }

                }
                else
                {
                    Status a = pe[_IndexEingangslager].GetStatus();
                    Produktionseinrichtung b = pe[_IndexEingangslager];


                    _Internal.Ausgabe(Ziel NICHT gefunden, überprüfe Eingangslager");
                    if (pe[_IndexEingangslager].GetStatus() == Status.ABHOLBEREIT || pe[_IndexEingangslager].GetStatus() == Status.INTERAKTIONSBEREIT && _AktuellesTeil == null)
                    {
                        FahreZu(pe[_IndexEingangslager].GetPosition());

                        //_Internal.Ausgabe("fahre zu Eingangslager");

                        if (_AktuellesTeil.GetNaechstenSchritt() == Verarbeitungsschritt.EINLAGERN && pe[_IndexEingangslager].GetStatus() != Status.ABHOLBEREIT)
                        {
                            FahreZu(pe[_IndexAusgangslager].GetPosition());
                        }
                        else
                        {
                            Produktionseinrichtung _FertigungsinselMitFaehigkeit = pe.Find(x => x.HatFaehigkeit(_AktuellesTeil.GetNaechstenSchritt()) == true);
                            _Internal.Ausgabe("suche Fertigungsinsel mit Faehigkeit");
                            if (_FertigungsinselMitFaehigkeit.GetStatus() == Status.EMPFANGSBEREIT)
                            {
                                FahreZu(_FertigungsinselMitFaehigkeit.GetPosition());
                                _Internal.Ausgabe("Fahre zu Fertigungsinsel mit Faehigkeit");
                            }
                            if (_FertigungsinselMitFaehigkeit.GetStatus() != Status.EMPFANGSBEREIT && pe[_IndexEingangslager].GetStatus() != Status.ABHOLBEREIT)
                            {
                                FahreZu(pe[_IndexEingangslager].GetPosition());
                                _Internal.Ausgabe("keine Fertigungsinsel EMPFANGSBEREIT, fahre zu Eingangslager");
                            }
                            if (pe[_IndexAusgangslager].GetStatus() != Status.ABHOLBEREIT) { FahreZu(pe[_IndexAusgangslager].GetPosition()); }
                            else { Ende = true; }
                        }
                    }
                    if (pe[_IndexInsel1].GetStatus() == Status.EMPFANGSBEREIT && pe[_IndexInsel2].GetStatus() == Status.EMPFANGSBEREIT && pe[_IndexInsel3].GetStatus() == Status.EMPFANGSBEREIT && pe[_IndexEingangslager].GetStatus() == Status.EMPFANGSBEREIT && _AktuellesTeil == null)
                    {
                        Ende = true;
                    }
                    _Internal.Ausgabe("leer");

                }

            }
        }
    }

    public class Teil
    {
        private string _Id;
        private List<Verarbeitungsschritt> _Rezept;
        public Teil(string id, List<Verarbeitungsschritt> rezept) { _Rezept = rezept; _Id = id; }
        public Verarbeitungsschritt GetNaechstenSchritt()
        {
            _Internal.Ausgabe("Naechster Schritt:" + Convert.ToString(_Rezept[0]));
            return _Rezept[0];
        }
        public void EntferneFertigenSchritt()
        {
            _Internal.Ausgabe("Der Schritt" + Convert.ToString(_Rezept[0]) + "wurde gelöscht");
            _Rezept.RemoveAt(0);
        }
        public string GetId()
        {
            return _Id;
        }
    }


    public class Eingangslager : Lager
    {
        public Eingangslager(string name, string pos) : base(name, pos) { InitialisiereBestand(@"../../../Eingangslager.csv"); }
        private void InitialisiereBestand(string pfad)
        {
            _Internal.Ausgabe("Eingangslager-Konfiguration wird eingelesen");

            StreamReader? reader = null;
            try
            {
                reader = new StreamReader(pfad);
                while (!reader.EndOfStream)
                {
                    String line = reader.ReadLine();

                    String[] values = line.Split(';', ',');

                    List<Verarbeitungsschritt> schritte = new List<Verarbeitungsschritt>();

                    foreach (string element in values)
                    {
                        if (element.Contains("BOHREN")) { schritte.Add(Verarbeitungsschritt.BOHREN); }
                        if (element.Contains("FRAESEN")) { schritte.Add(Verarbeitungsschritt.FRAESEN); }
                        if (element.Contains("SCHWEISSEN")) { schritte.Add(Verarbeitungsschritt.SCHWEISSEN); }
                        if (element.Contains("LACKIEREN")) { schritte.Add(Verarbeitungsschritt.LACKIEREN); }
                        if (element.Contains("EINLAGERN")) { schritte.Add(Verarbeitungsschritt.EINLAGERN); }
                    }

                    Teil teil = new Teil(values[0], schritte);
                    if (teil != null) { _Bestand.Enqueue(teil); }
                }
                reader.Close();

            }
            catch (FileNotFoundException ioex)
            {
                Console.WriteLine(ioex.Message);
                Console.WriteLine("Überprüfen Sie, die relativen Pfade! Im Vorgabeprojekt sollten diese Korrekt sein," +
                    "wenn das bei Ihnen nicht so ist, melden Sie sich bei einem Tutor oder im Forum! ");
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
            }
            _AktuellerStatus = Status.ABHOLBEREIT;
        }
        public Teil GetTeil()
        {
            if (_Bestand.Count == 1) { _AktuellerStatus = Status.EMPFANGSBEREIT; return _Bestand.Dequeue(); }
            if (_Bestand.Count > 1) { _AktuellerStatus = Status.INTERAKTIONSBEREIT; return _Bestand.Dequeue(); }
            return null;
        }
    }


    public class Ausgangslager : Lager
    {
        public Ausgangslager(string name, string pos) : base(name, pos) { }
    }
}
